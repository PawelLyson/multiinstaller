#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.2
 Author:         Paweł Łysoń

 Script Function:
	Automatyzacja Instalacji WinRara 5.21

#ce ----------------------------------------------------------------------------


main()

Func main()
   $WinClass = "[CLASS:#32770]"
   $PID = Run("WinRar521.exe")

   Sleep(3000)
   $hWnd = WinWait($WinClass)

   ControlClick($hWnd,"",1)

   Sleep(3000)
   $hWnd = WinWait("Instalacja WinRARa")

   If ControlCommand($hWnd, "", 5113, "IsChecked") Then
       ControlCommand($hWnd, "", 5113, "Uncheck")
   EndIf
   If ControlCommand($hWnd, "", 5112, "IsChecked") Then
       ControlCommand($hWnd, "", 5112, "Uncheck")
	EndIf
   ControlClick($hWnd,"",28)

   Sleep(3000)
   $hWnd = WinWait("Instalacja WinRARa")
   ControlClick($hWnd,"",1)

   Sleep(1000)
   ProcessClose("Uninstall.exe")
   ProcessWaitClose($PID)
EndFunc