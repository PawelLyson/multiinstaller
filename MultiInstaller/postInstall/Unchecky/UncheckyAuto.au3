#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.2
 Author:         Pawe� �yso�

 Script Function:
	Autmoatyzacja instalacji Unchecky

#ce ----------------------------------------------------------------------------
#RequireAdmin

main()

Func main()
   $sClass = "[CLASS:Unchecky Setup]"
   $PID = Run("Unchecky.exe")

   Sleep(1000)
   $hWnd = WinWait($sClass)

   ControlClick($hWnd,"", 1)
   Sleep(1000)
   While 1
   If ControlCommand($hWnd,"",1,"IsVisible","") Then
	  ControlClick($hWnd,"", 1)
	  ExitLoop
   EndIf
   WEnd


   ProcessWaitClose($PID)
EndFunc