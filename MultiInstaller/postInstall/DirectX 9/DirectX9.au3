#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.2
 Author:         Paweł Łysoń

 Script Function:
	Aumatyzuje instalacje Dx9

#ce ----------------------------------------------------------------------------

#RequireAdmin

main()

Func main()

   $WinClass = "[CLASS:#32770]"
   $PID = Run("DirectX9.29\DXSETUP.exe")
   $hWnd = WinWait($WinClass)

   Sleep(2000)
   While 1 ; Zakceptowanie regulaminu i poczekanie na aktywacje przycisku Dalej
	  ControlClick($WinClass,"",5009)
	  If ControlCommand($WinClass,"","[CLASS:Button; INSTANCE:4]","IsEnabled", "") Then ExitLoop
	  Sleep(100);
   WEnd
   ControlClick($WinClass,"", 12324)

   Sleep(2000)
   While 1 ; Poczekanie na aktywacje przycisku Dalej aby rozpocząc instalacje
	  If ControlCommand($WinClass,"","[CLASS:Button; INSTANCE:4]","IsEnabled", "") Then ExitLoop
	  Sleep(100)
   WEnd
   ControlClick($WinClass,"", 12324)

   Sleep(5000)
   While 1 ; Poczekanie na aktywacje przycisku Zakończ aby rozpocząc Zakończyć instalacje
	  If ControlCommand($WinClass,"","[CLASS:Button; INSTANCE:5]","IsVisible", "") Then ExitLoop
	  Sleep(100)
   WEnd
   ControlClick($WinClass,"", 12325)

   ProcessWaitClose($PID)
EndFunc
