#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.2
 Author:         Pawel Lyson

 Script Function:
	Program będzie dodawany do instalki windowsa i po procesie instalacji (ostatnia faza) będzie intalował programy z katalogu(postInstall) na pendrive.
	Będzie obsługiwał przełączniki oraz będzie posiadał mały plik konifuracyjny.
	Każdy program będzie w podfolderze.

#ce ----------------------------------------------------------------------------


#include <GUIConstantsEx.au3>
#include <ProgressConstants.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <String.au3>
#include <File.au3>

#RequireAdmin

Global $configFileSoft = "autoInstall.ini"

Func getFolderPostInstall()
   $aDrivers = DriveGetDrive("ALL")
   For $drive in $aDrivers
	  $folder = $drive & "\postInstall"
	  If FileExists($folder) Then
		 Return $folder
	  EndIf
   Next
   MsgBox(0x40030,"ERROR","Nie znaleziono programów do zainstalowania",10)
   Exit
EndFunc

Func installSoftware($folderPath)
   $configFullPath = $folderPath & "\" & $configFileSoft
   If FileExists($configFullPath) Then
	  $section = IniReadSection($configFullPath,@OSArch)
	  If (IsArray($section)) Then
		 $filename = $section[1][1]
		 $parameters = $section[2][1]
		 If StringRight($filename,4) = ".exe" And Not FileExists($configFullPath) Then
			MsgBox(0x40030,"ERROR","Plik instalatora nie istnieje")
		 Else
			;MsgBox(0x40000,"",$filename & @CRLF & $parameters)
			InstallAndWait($filename ,$parameters,$folderPath)
		 EndIf
	  Else
		 MsgBox(0x40030,"ERROR","Nie obsługiwany system "& @OSArch,10)
	  EndIf
   Else
	  MsgBox(0x40030,"ERROR","Brak pliku konfiguracyjnego",10)
   EndIf
EndFunc

$isQuestionForInterrupted = false
$isInterrupted = false

HotKeySet("{F9}", "changeQuestionForInterrupted")

Func changeQuestionForInterrupted()
   $isQuestionForInterrupted = true
EndFunc

$maxTime = 10 ; sekund
Func InstallAndWait($filename , $parameters,$folderPath)
   $processPID = ShellExecute($filename ,$parameters, $folderPath)
   $isQuestionForInterrupted = false
   $clockForInstallation = 0
   $AmountNoUserResponse = 0;
   Do
	  Sleep(100)
	  $installatorIsAlive = ProcessExists($processPID)
   If $isQuestionForInterrupted Or $maxTime*10 <= $ClockForInstallation Then
	  $IsTooLongQuestion = MsgBox(0x34,"Instalacja","Instalacji trwa zbyt długo." & @CRLF & "Czy przerwac operacje?", 10)
	  If $IsTooLongQuestion == $IDYES Or 2 <= $AmountNoUserResponse Then
		 MsgBox(0,"Przerwanie instalacji","Instalacja programu " & $filename &" proces została przerwana.", 10)
		 _ProcessCloseEx($processPID)
		 Sleep(1000)
		 ExitLoop
	  ElseIf $IsTooLongQuestion == $IDTIMEOUT Then
		 $AmountNoUserResponse += 1
	  EndIf
		 $isQuestionForInterrupted = false
		 $clockForInstallation = 0
   EndIf
   $clockForInstallation += 1
   Until Not($installatorIsAlive > 0)

EndFunc

Func _ProcessCloseEx($sPID)
    If IsString($sPID) Then $sPID = ProcessExists($sPID)
    If Not $sPID Then Return SetError(1, 0, 0)

    Return Run(@ComSpec & " /c taskkill /F /PID " & $sPID & " /T", @SystemDir, @SW_HIDE)
EndFunc

Func GUIprogress()
   #Region ### START Koda GUI section ### Form=E:\Dropbox\AutoIT2\koda_1.7.3.0\Forms\ProgressInstalation.kxf
   $InstalatorSilent = GUICreate("Instalator oprogramowania", 405, 121, -1, -1, BitOR($WS_SYSMENU,$WS_POPUP), BitOR($WS_EX_TOPMOST,$WS_EX_WINDOWEDGE))
   WinSetOnTop($instalatorSilent,"",0)
   $ProgressBar = GUICtrlCreateProgress(8, 72, 390, 17)
   $Label = GUICtrlCreateLabel("Instalacja programów:", 8, 47, 107, 17)
   $AmountProgramsLabel = GUICtrlCreateLabel("", 120, 47, 71, 17)
   $Label1 = GUICtrlCreateLabel("Instalowany program:", 8, 96, 104, 17)
   $NameLabel = GUICtrlCreateLabel("", 112, 96, 291, 17)
   $Label2 = GUICtrlCreateLabel("Kończenie instalacji systemu Windows.", 8, 8, 188, 17)
   $Label3 = GUICtrlCreateLabel("Instalowanie dodatkowego oprogramowania.", 8, 27, 215, 17)
   GUISetState(@SW_SHOW)
   #EndRegion ### END Koda GUI section ###

   $folder = getFolderPostInstall()
   $aListFolders = _FileListToArray($folder, "*",2,True)

   $amountPrograms = UBound($aListFolders)-1
   $oneStep = Floor(100/$amountPrograms)

   GUICtrlSetData($AmountProgramsLabel, "1 z "&$amountPrograms)

	$actualProgress = $oneStep

   For $i = 1 To $amountPrograms
	  $startName = StringInStr($aListFolders[$i],"\",0,-1)
	  $softwareName = StringTrimLeft($aListFolders[$i],$startName)
	  GUICtrlSetData($AmountProgramsLabel, $i & " z " & $amountPrograms)
	  GUiCtrlSetData($ProgressBar,$actualProgress)
	  GUICtrlSetData($NameLabel,$softwareName)
	  installSoftware($aListFolders[$i])
	  ;Sleep(2000)
	  $actualProgress +=  $oneStep
   Next
   GUIDelete($InstalatorSilent)
   MsgBox(0,"Instalacja zakończona","Instalacja systemu windows oraz dodatkowego oprogramowanai została zakończona",10)
   Exit
EndFunc

Func main()
   GUIprogress()
EndFunc

main() 